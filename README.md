# sails-openshift
## a Sails application 

### create app
rhc app create sails nodejs-0.10 mongodb-2.2 rockmongo-1.1 cron-1.4 --from-code https://lopelex@bitbucket.org/lopelex/sails_openshift.git

### setup env 
rhc app env-var add OPENSHIFT_NODEJS_POLL_INTERVAL="5007 -w api,assets,config,node_modules,Gruntfile.js,app.js" -a sails
