module.exports = {

    appName: 'sails-openshift',

    host: process.env.OPENSHIFT_NODEJS_IP,

    port: process.env.OPENSHIFT_NODEJS_PORT,

    environment: 'production',

    xPoweredBy: 'sails',

    forceSSL: true,

    dns: process.env.OPENSHIFT_GEAR_DNS
};

// force https
/*
module.exports.express = {


    customMiddleware: function(app) {

        app.use(function(req, res, next) {

            if (!process.env.OPENSHIFT_GEAR_DNS) {
                return next();
            }

            if (req.headers['x-forwarded-proto'] === 'https') {
                next();
            } else {
                res.redirect('https://' + process.env.OPENSHIFT_GEAR_DNS + req.url);
            };
        });
    }
};
*/
