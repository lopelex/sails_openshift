module.exports = function(sails) {

    return {

        routes: {

            before: {

                '/*': function(req, res, next) {

                    if (sails.config.xPoweredBy) {
                        res.header('X-Powered-By', sails.config.xPoweredBy);
                    } else {
                        res.removeHeader('x-powered-by');
                    }
                    next();
                }
            }
        }
    };
};
