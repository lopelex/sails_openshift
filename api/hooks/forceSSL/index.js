module.exports = function(sails) {

    return {

        routes: {

            before: {

                '/*': function(req, res, next) {

                    if (!sails.config.forceSSL) {
                        return next();
                    }

                    if (req.header('x-forwarded-proto') === 'https') {
                        next();
                    } else {
                        res.redirect('https://' + sails.config.DNS + req.url);
                    };
                }
            }
        }
    };
};
